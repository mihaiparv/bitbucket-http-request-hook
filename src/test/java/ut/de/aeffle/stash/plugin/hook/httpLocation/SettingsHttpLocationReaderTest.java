/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.httpLocation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import org.junit.Before;
import org.junit.Test;

import ut.de.aeffle.stash.plugin.hook.testHelpers.RepositoryHookRequestMockFactory;


public class SettingsHttpLocationReaderTest {
	private final RepositoryHookRequestMockFactory repositoryHookRequestFactory = new RepositoryHookRequestMockFactory();

    @Before
    public void beforeTestClearSettings() {
    	repositoryHookRequestFactory.clear();
    }

 
	@Test
	public void testGetUrl1() {
        //GIVEN
		repositoryHookRequestFactory.setUrl( 1, "http://aeffle.de");

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

		//THEN
        assertThat(firstHttpLocation.getUrlTemplate())
                .isEqualTo("http://aeffle.de");
	}

	@Test
	public void testGetUser1() {
        //GIVEN
		repositoryHookRequestFactory.setUser(1, "john.doe");

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

        //THEN
        assertThat(firstHttpLocation.getUser())
                .isEqualTo("john.doe");
	}

	@Test
	public void testGetPass1() {
        //GIVEN
		repositoryHookRequestFactory.setPass(1, "secret");

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

        //THEN
        assertThat(firstHttpLocation.getPass()).isEqualTo("secret");
	}

	@Test
	public void testGetUseAuthNullV1() {
        //GIVEN
		repositoryHookRequestFactory.setOldUseAuth(null);

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

		//THEN
        assertThat(firstHttpLocation.isAuthEnabled())
                .isFalse();
	}
	
	@Test
	public void testGetUseAuthFalseV1() {
		//GIVEN
        repositoryHookRequestFactory.setOldUseAuth(false);

        //WHEN
        HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

        //THEN
        assertThat(firstHttpLocation.isAuthEnabled()).isFalse();
	}

	@Test
	public void testGetUseAuthTrueV1() {
        //GIVEN
		repositoryHookRequestFactory.setOldUseAuth(true);

		//WHEN
		HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

		//THEN
		assertThat(firstHttpLocation.isAuthEnabled()).isTrue();
	}

	@Test
	public void testGetUseAuthTrueV2() {
        //GIVEN
		repositoryHookRequestFactory.setVersion("2");
		repositoryHookRequestFactory.setUseAuth(1, true);

		//WHEN
		HttpLocation firstHttpLocation = repositoryHookRequestFactory.getFirstHttpLocation();

		//THEN
		assertThat(firstHttpLocation.isAuthEnabled()).isTrue();
	}
	
	@Test
	public void testgetNumberOfHttpLocationsNull() {
        //GIVEN
		repositoryHookRequestFactory.setLocationCount(null);

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();

		//THEN
		assertThat(httpLocations.size()).isEqualTo(1);
   }
	
	@Test
	public void testgetNumberOfHttpLocations1() {
        //GIVEN
		repositoryHookRequestFactory.setLocationCount("1");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();

		//THEN
        assertThat(httpLocations.size()).isEqualTo(1);
   }
	
	@Test
	public void testgetNumberOfHttpLocations3() {
        //GIVEN
		repositoryHookRequestFactory.setLocationCount("3");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();

		//THEN
        assertThat(httpLocations.size()).isEqualTo(3);
    }


	@Test
	public void testGetUrl2() {
        //GIVEN
		repositoryHookRequestFactory.setLocationCount("2");
		repositoryHookRequestFactory.setUrl(2, "http://aeffle.de");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();
		HttpLocation httpLocation = repositoryHookRequestFactory.getHttpLocation(2);

		//THEN
		assertThat(httpLocations.size()).isGreaterThanOrEqualTo(2)
                .withFailMessage("LocationCount shouldn't be smaller than 2.");

		assertThat(httpLocation.getUrlTemplate()).isEqualTo("http://aeffle.de");
	}
	
	@Test
	public void testGetUser2() {
        //GIVEN
		repositoryHookRequestFactory.setLocationCount("2");
		repositoryHookRequestFactory.setUser(2, "john.doe");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();
		HttpLocation httpLocation = repositoryHookRequestFactory.getHttpLocation(2);

		//THEN
        assertThat(httpLocations.size()).isGreaterThanOrEqualTo(2)
                .withFailMessage("LocationCount shouldn't be smaller than 2.");

        assertThat(httpLocation.getUser()).isEqualTo("john.doe");
	}
	
	@Test
	public void testGetPass2() {
        //GIVEN
		repositoryHookRequestFactory.setLocationCount("2");
		repositoryHookRequestFactory.setPass(2, "secret");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();
		HttpLocation httpLocation = repositoryHookRequestFactory.getHttpLocation(2);

        //THEN
        assertThat(httpLocations.size()).isGreaterThanOrEqualTo(2)
                .withFailMessage("LocationCount shouldn't be smaller than 2.");

		assertThat(httpLocation.getPass()).isEqualTo("secret");
	}
	
	@Test
	public void testManyLocations() {
        //GIVEN
		repositoryHookRequestFactory.setVersion("2");
		repositoryHookRequestFactory.setLocationCount("3");
		
		repositoryHookRequestFactory.setUrl( 1, "http://aeffle.de/1");
		repositoryHookRequestFactory.setUrl( 2, "http://aeffle.de/2");
		repositoryHookRequestFactory.setUrl( 3, "http://aeffle.de/3");

		repositoryHookRequestFactory.setUseAuth(1, true);
		repositoryHookRequestFactory.setUseAuth(2, true);
		repositoryHookRequestFactory.setUseAuth(3, true);

		repositoryHookRequestFactory.setUser(1, "john.doe1");
		repositoryHookRequestFactory.setUser(2, "john.doe2");
		repositoryHookRequestFactory.setUser(3, "john.doe3");

		repositoryHookRequestFactory.setPass(1, "secret1");
		repositoryHookRequestFactory.setPass(2, "secret2");
		repositoryHookRequestFactory.setPass(3, "secret3");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookRequestFactory.getHttpLocations();
		HttpLocation httpLocation1 = repositoryHookRequestFactory.getHttpLocation(1);
		HttpLocation httpLocation2 = repositoryHookRequestFactory.getHttpLocation(2);
		HttpLocation httpLocation3 = repositoryHookRequestFactory.getHttpLocation(3);

        //THEN
		assertThat(httpLocations.size()).isGreaterThanOrEqualTo(3)
                .withFailMessage("LocationCount shouldn't be smaller than 3.");

		assertThat(httpLocation1.getUrlTemplate()).isEqualTo("http://aeffle.de/1");
		assertThat(httpLocation2.getUrlTemplate()).isEqualTo("http://aeffle.de/2");
		assertThat(httpLocation3.getUrlTemplate()).isEqualTo("http://aeffle.de/3");

		assertThat(httpLocation1.isAuthEnabled()).isTrue();
		assertThat(httpLocation2.isAuthEnabled()).isTrue();
		assertThat(httpLocation3.isAuthEnabled()).isTrue();

		assertThat(httpLocation1.getUser()).isEqualTo("john.doe1");
		assertThat(httpLocation2.getUser()).isEqualTo("john.doe2");
		assertThat(httpLocation3.getUser()).isEqualTo("john.doe3");

		assertThat(httpLocation1.getPass()).isEqualTo("secret1");
		assertThat(httpLocation2.getPass()).isEqualTo("secret2");
		assertThat(httpLocation3.getPass()).isEqualTo("secret3");
	}
}