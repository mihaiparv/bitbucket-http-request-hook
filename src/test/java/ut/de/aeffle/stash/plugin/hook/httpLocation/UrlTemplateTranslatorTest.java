/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.httpLocation;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.hook.repository.RepositoryHookRequest;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import de.aeffle.stash.plugin.hook.http.location.UrlTemplateTranslator;
import org.junit.Before;
import org.junit.Test;
import ut.de.aeffle.stash.plugin.hook.testHelpers.*;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;


public class UrlTemplateTranslatorTest {

    private final ApplicationPropertiesServiceMockFactory applicationPropertiesServiceMockFactory = new ApplicationPropertiesServiceMockFactory();
    private final RepositoryHookRequestMockFactory contextFactory = new RepositoryHookRequestMockFactory();
    private final AuthenticationContextMockFactory authenticationContextFactory = new AuthenticationContextMockFactory();
    private final RefChangeMockFactory refChangeMockFactory = new RefChangeMockFactory();

    private ApplicationPropertiesService applicationPropertiesService;
    private AuthenticationContext authenticationContext;
    private RepositoryHookRequest repositoryHookRequest;

    @Before
    public void beforeTestClearSettings() {
        applicationPropertiesServiceMockFactory.clear();
        contextFactory.clear();
        authenticationContextFactory.clear();
        refChangeMockFactory.clear();

        applicationPropertiesService = applicationPropertiesServiceMockFactory.getContext();
        authenticationContext = authenticationContextFactory.getContext();
        repositoryHookRequest = contextFactory.getRequest();
    }

    @Test
    public void testTransformWithBaseUrl() {
        //GIVEN
        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("${baseUrl}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://example.com/stash");
    }

    @Test
    public void testTransformWithBaseUrlProtocol() {
        //GIVEN
        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("${baseUrl.protocol}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http");
    }

    @Test
    public void testTransformWithBaseUrlHost() {
        //GIVEN
        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("${baseUrl.host}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("example.com");
    }

    @Test
    public void testTransformWithBaseUrlPath() {
        //GIVEN
        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("${baseUrl.path}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("/stash");
    }

    @Test
    public void testTransformWithDisplayName() {
        //GIVEN
        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                new AuthenticationContextMockBuilder()
                        .setDisplayName("John_Doe")
                        .build(),
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("http://doe.com/${user.displayName}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/John_Doe");
    }

    @Test
    public void testTransformWithSpaceInDisplayName() {
        //GIVEN
        authenticationContextFactory.setDisplayName("John Doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                (RefChange) null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("http://doe.com/${user.displayName}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/John+Doe");
    }


    @Test
    public void testTransformWithUserName() {
        //GIVEN
        authenticationContextFactory.setName("john.doe");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();
        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);


        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("http://doe.com/${user.name}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/john.doe");
    }

    @Test
    public void testTransformWithEmail() {
        //GIVEN
        authenticationContextFactory.setEmailAddress("john@doe.de");
        AuthenticationContext authenticationContext = authenticationContextFactory.getContext();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("http://doe.com/${user.email}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/john@doe.de");
    }

    @Test
    public void testTransformWithRepository() {
        //GIVEN
        contextFactory.setRepositoryId(1);
        contextFactory.setRepositoryName("Test Repository");
        contextFactory.setRepositorySlug("REPO");

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("http://doe.com/${repository.id}/${repository.name}/${repository.slug}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/1/Test+Repository/REPO");
    }

    @Test
    public void testTransformWithProject() {
        //GIVEN
        contextFactory.setProjectKey("PROJECT");
        contextFactory.setProjectName("Test Project");

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("http://doe.com/${project.name}/${project.key}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/Test+Project/PROJECT");
    }


    @Test
    public void testTransformWithProjectLower() {
        //GIVEN
        contextFactory.setProjectKey("PROJECT");

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                null);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .urlTemplate("${project.key.lower}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("project");
    }

    @Test
    public void testTransformWithRefChange() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/master");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("cafe");
        RefChange refChange = refChangeMockFactory.getRefChange();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                authenticationContext,
                null,
                repositoryHookRequest,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .httpMethod("GET")
                .user("john.doe")
                .urlTemplate("http://doe.com/${refChange.refId}/${refChange.fromHash}/${refChange.toHash}/${refChange.type}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/refs/heads/master/affe/cafe/UPDATE");
        assertThat(httpLocationTranslated.getHttpMethod()).isEqualTo("GET");
        assertThat(httpLocationTranslated.getUser()).isEqualTo("john.doe");
    }

    @Test
    public void testUrlEncodeRefChangeName() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/feature/test");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("cafe");
        RefChange refChange = refChangeMockFactory.getRefChange();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                null,
                null,
                null,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .httpMethod("GET")
                .user("john.doe")
                .urlTemplate("http://doe.com/${refChange.name}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/feature%2Ftest");
        assertThat(httpLocationTranslated.getHttpMethod()).isEqualTo("GET");
        assertThat(httpLocationTranslated.getUser()).isEqualTo("john.doe");
    }


    @Test
    public void testEmptyToRef() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/feature/test");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash(null);
        RefChange refChange = refChangeMockFactory.getRefChange();
        CommitService commitService = new CommitServiceMockBuilder().build();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                null,
                commitService,
                repositoryHookRequest,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .httpMethod("GET")
                .user("john.doe")
                .urlTemplate("http://doe.com/${refChange.latest.message}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/");
    }

    @Test
    public void testAllZeroToRef() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/feature/test");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("0000000000000000000000000000000000000000");
        RefChange refChange = refChangeMockFactory.getRefChange();
        CommitService commitService = new CommitServiceMockBuilder().build();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                null,
                commitService,
                repositoryHookRequest,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .httpMethod("GET")
                .user("john.doe")
                .urlTemplate("http://doe.com/${refChange.latest.timeStamp.iso}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        String expectedUrlString = "http://doe.com/" + DateTimeFormatter.ISO_OFFSET_DATE_TIME
                .withZone(ZoneId.systemDefault())
                .format(new Date(0L).toInstant());
        assertThat(httpLocationTranslated.getUrl()).isEqualTo(expectedUrlString);
    }


    @Test
    public void testLatestCommitMessage() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/feature/test");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("cafe");
        RefChange refChange = refChangeMockFactory.getRefChange();
        CommitService commitService = new CommitServiceMockBuilder().build();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                null,
                commitService,
                repositoryHookRequest,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .httpMethod("GET")
                .user("john.doe")
                .urlTemplate("http://doe.com/${refChange.latest.message}")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/hello-world");
    }

    @Test
    public void testPostDataTranslation() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/master");
        RefChange refChange = refChangeMockFactory.getRefChange();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                null,
                null,
                null,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .postDataTemplate("{ refId: '${refChange.refId}' }")
                .build();
        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getPostData()).isEqualTo("{ refId: 'refs/heads/master' }");
    }

    @Test
    public void testTransformerCopiesAllValues() {
        //GIVEN
        refChangeMockFactory.setRefId("refs/heads/master");
        refChangeMockFactory.setFromHash("affe");
        refChangeMockFactory.setToHash("cafe");
        RefChange refChange = refChangeMockFactory.getRefChange();

        UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                null,
                null,
                null,
                refChange);

        //WHEN
        HttpLocation httpLocation = HttpLocation.builder()
                .httpMethod("PUT")
                .postDataTemplate("foo=${refChange.toHash}")
                .authEnabled(true)
                .user("john.doe")
                .pass("secret")
                .branchFilter("branchFilter")
                .tagFilter("tagFilter")
                .userFilter("userFilter")
                .urlTemplate("http://doe.com/${refChange.refId}/${refChange.fromHash}/${refChange.toHash}/${refChange.type}")
                .build();

        HttpLocation httpLocationTranslated = translator.translate(httpLocation);

        //THEN
        assertThat(httpLocationTranslated.getUrl()).isEqualTo("http://doe.com/refs/heads/master/affe/cafe/UPDATE");
        assertThat(httpLocationTranslated.getHttpMethod()).isEqualTo("PUT");
        assertThat(httpLocationTranslated.getPostData()).isEqualTo("foo=cafe");

        assertThat(httpLocationTranslated.isAuthEnabled()).isTrue();
        assertThat(httpLocationTranslated.getUser()).isEqualTo("john.doe");
        assertThat(httpLocationTranslated.getPass()).isEqualTo("secret");

        assertThat(httpLocationTranslated.getBranchFilter()).isEqualTo("branchFilter");
        assertThat(httpLocationTranslated.getTagFilter()).isEqualTo("tagFilter");
        assertThat(httpLocationTranslated.getUserFilter()).isEqualTo("userFilter");
    }
}