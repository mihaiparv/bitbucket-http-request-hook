/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.http.location;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HttpLocation {
    final private String urlTemplate;
    final private String url;
    final private boolean sslValidationDisabled;
    final private String httpMethod;

    final private String postContentType;
    final private String postDataTemplate;
    final private String postData;

    final private boolean authEnabled;
    final private String user;
    final private String pass;

    final private String branchFilter;
    final private String tagFilter;
    final private String userFilter;

    public String toString() {
        return (url != null ? url : urlTemplate);
    }
}