/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.http.agent;

import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;


public class OldHttpAgent {
    private static final Logger log = LoggerFactory.getLogger(OldHttpAgent.class);

    private final String USER_AGENT = "Mozilla/5.0";

    private final String urlString;
    private final Boolean skipSslValidation;
    private final String httpMethod;
    private final String postContentType;
    private final String postData;

	private final String user;
	private final String pass;
	private final Boolean useAuth;

    private URLConnection urlConnection;


    @Builder
    @Data
    public static class Result {
        private int statusCode;
        private String message;
    }


    public OldHttpAgent(HttpLocation httpLocation) {
		urlString = httpLocation.getUrl();
        skipSslValidation = httpLocation.isSslValidationDisabled();

        httpMethod = httpLocation.getHttpMethod();
        postContentType = httpLocation.getPostContentType();
		postData = httpLocation.getPostData();

        useAuth = httpLocation.isAuthEnabled();
        user = httpLocation.getUser();
	    pass = httpLocation.getPass();
	    
		log.info("Http request with URL: " + urlString + " (" + httpMethod + ")");
	}


	public Result doRequest() {
        Result result = new Result(-1, "N/A");
		try {
			if (urlConnection == null) {
				URL url = new URL(urlString);

                urlConnection = url.openConnection();
                log.info("doRequest: " + url.getHost());

                if (urlConnection instanceof HttpsURLConnection && skipSslValidation) {
                    log.info("Skip SSL Validation for " + urlString);
                    skipSslValidation((HttpsURLConnection) urlConnection);
                }
            }

			urlConnection.setReadTimeout(5000);
            urlConnection.setRequestProperty("User-Agent", USER_AGENT);

            if (useAuth) {
				authenticate(user, pass);
			}

            if (httpMethod == null || httpMethod.equals("GET") || httpMethod.equals("")) {
                log.info("Start with the GET: " );
                result = doGet();
            } else {
                if (httpMethod.equals("POST") || httpMethod.equals("PUT")) {
                    log.info("Start with the POST or PUT");
                    result= doPost();
                }
            }
		}
        catch (MalformedURLException e) {
            result.message = "Malformed URL: " + e.getMessage();
            log.error("Malformed URL:" + e + " - " + e.getMessage());
        }
        catch (IOException e) {
            result.message = "Some IO exception occured: " + e.getMessage();
            log.error("Some IO exception occured " + e + " - " + e.getMessage());
        }
        catch (Exception e) {
		    result.message = "Something else went wrong: " + e.getMessage();
            log.error("Something else went wrong: ", e);
        }
        return result;
	}

    public void skipSslValidation(HttpsURLConnection httpsURLConnection) {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };

        try {
            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            httpsURLConnection.setSSLSocketFactory(sc.getSocketFactory());

        } catch (NoSuchAlgorithmException|KeyManagementException e) {
            log.info("Could not build all trusting SSLSocketFactory", e);
        }

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        httpsURLConnection.setHostnameVerifier(allHostsValid);
    }



    private void authenticate(String user, String pass) {
        log.info("Authentication was enabled with user: " + user);

        // build the auth string
        String authString = user + ":" + pass;
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));

        urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
    }


    private Result doGet() throws IOException {
        ((HttpURLConnection)urlConnection).setInstanceFollowRedirects(true);
        HttpURLConnection.setFollowRedirects(true);
        urlConnection.connect();

        int responseCode = getResponseCode();
        String responseBody = getResponseBody();

        ((HttpURLConnection)urlConnection).disconnect();

        return new Result(responseCode, responseBody);
    }

    private Result doPost() throws IOException {
        ((HttpURLConnection)urlConnection).setRequestMethod(httpMethod);
        urlConnection.setRequestProperty("charset", "utf-8");
        urlConnection.setRequestProperty("Content-Type", postContentType);
        urlConnection.setRequestProperty("Content-Length", "" + Integer.toString(postData.getBytes().length));
        urlConnection.setRequestProperty("Content-Language", "en-US");

        ((HttpURLConnection)urlConnection).setInstanceFollowRedirects(false);
        urlConnection.setUseCaches (false);
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        // Send post request
        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream ());
        wr.writeBytes(postData);
        wr.flush();
        wr.close();

        int responseCode = getResponseCode();
        String responseBody = getResponseBody();

        ((HttpURLConnection)urlConnection).disconnect();
        return new Result(responseCode, responseBody);
    }

    private int getResponseCode() throws IOException {
        int responseCode = ((HttpURLConnection)urlConnection).getResponseCode();

        if (responseCode < 200 || responseCode >= 300) {
            log.error("Problem with the HTTP connection with response code: " + responseCode);
        }
        return responseCode;
    }

    private String getResponseBody() throws IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                urlConnection.getInputStream()));
        String inputLine;
        StringBuffer body = new StringBuffer();

        while ((inputLine = buffer.readLine()) != null) {
            body.append(inputLine + "\n");
        }
        // close everything
        buffer.close();

        String content = body.toString();
        log.debug("HTTP response:\n" + content);

        return content;
    }

    // For testing purposes to inject a connection:
    public void setConnection(HttpsURLConnection httpsURLConnection) {
        this.urlConnection = httpsURLConnection;
    }

}
